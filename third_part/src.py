#import something if needed

import pandas as pd 

#exercie 1
product_list =  pd.read_json('data/products.json.gz')

def clean_cat(cat):
    return cat.strip(",NULL")

product_list.idappcat = product_list.idappcat.apply(clean_cat)



def import_raw_data():
    # todo exercise 2
    path = "data/"
    raw_data = pd.read_table(path + filename, sep=';')
    return raw_data


def process_data():
    # todo exercise 2
    path = "data/"
    asset_data = pd.read_csv(path + "back_office.csv.gz")
    processed_data = pd.concat([asset_data,raw_data])
    return  processed_data


def average_prices(processed_data,day):
    # todo exercise 3
    average_prices = processed_data.groupby("identifiantproduit")["prixproduit"].mean()

    df_avg = pd.DataFrame(average_prices)

    return df_avg.to_csv(f"Day {day} average prices")


def count_products_by_categories_by_day():
    # todo exercise 4
    DIprodCat = len(processed_data.groupby("p_id_cat")["p_id_cat"].count())  

    RETprodCat = len(processed_data.groupby("categorieenseigne")["categorieenseigne"].count())  

    return DIprodCat, RETprodCat


def average_products_by_categories():
    # todo exercise 4
    diAvgProd = len(processed_data.groupby(["pe_id"])["p_id_cat"]) / len(processed_data["p_id_cat"].unique())

    retAvgProd = len(processed_data.groupby(["identifiantproduit"])["categorieenseigne"])/len(processed_data["categorieenseigne"].unique())

    return diAvgProd, retAvgProd


if __name__ == '__main__':
    raw_data_20181017 = import_raw_data("17-10-2018.3880.gz")
    raw_data_20181018 = import_raw_data("17-10-2018.3880.gz")

    processed_data_20181017 = process_data(raw_data_20181017)
    processed_data_20181018 = process_data(raw_data_20181017)

    average_prices(processed_data_20181017,"2018-10-17")
    average_prices(processed_data_20181018,"2018-10-18")

    print(f"Day 1 counts :{count_products_by_categories_by_day(processed_data_20181017)}")
    print(f"Day 2 counts :{count_products_by_categories_by_day(processed_data_20181018)}")


    print(f"Day 1 avgs :{average_products_by_categories(processed_data_20181017)}")
    print(f"Day 2 avgs :{average_products_by_categories(processed_data_20181017)}")
