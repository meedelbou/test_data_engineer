def exercise_one():
    for i in range(1,101):
        print("ThreeFive" if i % 15 == 0 else "Five" if  i%5==0 else "Three" if i%3 ==0 else i  )



def exercise_two(arr):
    n = len(arr) + 1
    expected_sum = (n * (n+1)) // 2
    actual_sum = sum(arr)
    return expected_sum - actual_sum


def exercise_three(arr):

    pos = [ i for i in arr if i>0 ]
    pos.sort() 

    i=0
    j=0
    
    while i<len(pos) or j<len(arr):
        if arr[j] > 0:
            arr[j] = pos[i]
            i+=1
            j+=1
        else:
            j+=1

    return(arr)


