

class MySet(set):
    # todo exercise 1
    def __init__(self, set):
        self.items = set

    def __add__(self, new):
        return set(self.items + new.items)

    def __sub__(self, new):
        return set(self.items) - set(new.items)


def decorator_check_max_int(func):
    # todo exercise 2
    def inner(a, b):
        if func(a, b) < maxsize:
            return func(a, b)
        else:
            return maxsize

    return inner


@decorator_check_max_int
def add(a, b):
    return a + b


def ignore_exception(exception):
    # todo exercise 3
    def decorator(func):  
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception:
                return

        return wrapper 

    return decorator


@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""
    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap


class MetaInherList:
    # todo exercise 5
    def __new__(self, class_name, bases, attrs):
        for key, attr in attrs.items():
            if key == object:
                attr = object()
            else:
                attr = tuple(list())
        return type(class_name, bases, {'attrs': attr})


class Ex:
    x = 4


class ForceToList(Ex, metaclass=MetaInherList):
    pass

